# Deno Uno

a command line online game of uno.

## Environment

Deno: [1.11.2](https://deno.land/#installation)

## Tutorial

### play

```shell
deno run --unstable --allow-net https://gitee.com/oloshe/deno-uno/attach_files/756244/download/uno@0.1.0.js
```

> For convenience, the following command replaces the URL address with `uno.js`

### run as your local server

```shell
deno run --unstable --allow-net uno.js server
```

### connect other server

```shell
deno run --unstable --allow-net uno.js --host <host addr>
```

### control

press **W (up)** or **S (down)** to control the cursor

press **Enter** to confirm

## Screenshot

![menu](https://gitee.com/oloshe/deno-uno/raw/master/pics/WX20210628-211439.png)

![room](https://gitee.com/oloshe/deno-uno/raw/master/pics/WX20210628-211604.png)

![game](https://gitee.com/oloshe/deno-uno/raw/master/pics/WX20210628-211656.png)

![win](https://gitee.com/oloshe/deno-uno/raw/master/pics/WX20210628-211812.png)

## Have Fun

:)